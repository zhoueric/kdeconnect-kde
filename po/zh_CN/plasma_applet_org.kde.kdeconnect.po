msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-29 00:46+0000\n"
"PO-Revision-Date: 2022-11-19 14:48\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kdeconnect-kde/plasma_applet_org.kde."
"kdeconnect.pot\n"
"X-Crowdin-File-ID: 11807\n"

#: package/contents/ui/Battery.qml:26
#, kde-format
msgid "%1% charging"
msgstr "%1% 正在充电"

#: package/contents/ui/Battery.qml:26
#, kde-format
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:26
#, kde-format
msgid "No info"
msgstr "无信息"

#: package/contents/ui/Connectivity.qml:41
#, kde-format
msgid "Unknown"
msgstr "未知"

#: package/contents/ui/Connectivity.qml:51
#, kde-format
msgid "No signal"
msgstr "无信号"

#: package/contents/ui/DeviceDelegate.qml:51
#, kde-format
msgid "File Transfer"
msgstr "文件传送"

#: package/contents/ui/DeviceDelegate.qml:52
#, kde-format
msgid "Drop a file to transfer it onto your phone."
msgstr "将文件传送到您的手机上。"

#: package/contents/ui/DeviceDelegate.qml:87
#, kde-format
msgid "Virtual Display"
msgstr "虚拟显示器"

#: package/contents/ui/DeviceDelegate.qml:138
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/DeviceDelegate.qml:160
#, kde-format
msgid "Please choose a file"
msgstr "请选择文件"

#: package/contents/ui/DeviceDelegate.qml:169
#, kde-format
msgid "Share file"
msgstr "分享文件"

#: package/contents/ui/DeviceDelegate.qml:178
#, kde-format
msgid "Save As"
msgstr "另存为"

#: package/contents/ui/DeviceDelegate.qml:191
#, kde-format
msgid "Take a photo"
msgstr "拍摄照片"

#: package/contents/ui/DeviceDelegate.qml:206
#, kde-format
msgid "Ring my phone"
msgstr "让我的手机响铃"

#: package/contents/ui/DeviceDelegate.qml:224
#, kde-format
msgid "Browse this device"
msgstr "浏览此设备"

#: package/contents/ui/DeviceDelegate.qml:241
#, kde-format
msgid "SMS Messages"
msgstr "短信"

#: package/contents/ui/DeviceDelegate.qml:262
#, kde-format
msgid "Remote Keyboard"
msgstr "远程键盘"

#: package/contents/ui/DeviceDelegate.qml:278
#, kde-format
msgid "Notifications:"
msgstr "通知："

#: package/contents/ui/DeviceDelegate.qml:285
#, kde-format
msgid "Dismiss all notifications"
msgstr "清除所有通知"

#: package/contents/ui/DeviceDelegate.qml:324
#, kde-format
msgid "Reply"
msgstr "回复"

#: package/contents/ui/DeviceDelegate.qml:333
#, kde-format
msgid "Dismiss"
msgstr "忽略"

#: package/contents/ui/DeviceDelegate.qml:344
#, kde-format
msgid "Cancel"
msgstr "取消"

#: package/contents/ui/DeviceDelegate.qml:357
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1…"
msgstr "回复给 %1…"

#: package/contents/ui/DeviceDelegate.qml:373
#, kde-format
msgid "Send"
msgstr "发送"

#: package/contents/ui/DeviceDelegate.qml:397
#, kde-format
msgid "Run command"
msgstr "运行命令"

#: package/contents/ui/DeviceDelegate.qml:405
#, kde-format
msgid "Add command"
msgstr "添加命令"

#: package/contents/ui/FullRepresentation.qml:53
#, kde-format
msgid "No paired devices"
msgstr "没有配对的设备"

#: package/contents/ui/FullRepresentation.qml:53
#, kde-format
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "没有可用的配对设备"

#: package/contents/ui/FullRepresentation.qml:55
#, kde-format
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr "请在您的 Android 设备上安装 KDE Connect，让它能够与 Plasma 桌面集成。"

#: package/contents/ui/FullRepresentation.qml:59
#, kde-format
msgid "Pair a Device..."
msgstr "配对设备..."

#: package/contents/ui/FullRepresentation.qml:71
#, kde-format
msgid "Install from Google Play"
msgstr "从 Google Play 安装"

#: package/contents/ui/FullRepresentation.qml:81
#, kde-format
msgid "Install from F-Droid"
msgstr "从 F-Droid 安装"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "KDE Connect Settings..."
msgstr "KDE Connect 设置..."
